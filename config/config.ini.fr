# Planet configuration file

# The following rules apply for a new language planet:
#
# * The feeds contain the same content as one would put on planet.debian.org,
#   ie. mostly Debian related / from people involved in Debian
#
# * The feeds provide an own category with only the language for this planet.
#
# * At least 10 feeds have to be there before a new language gets added.
#
# * Language planets will appear as planet.debian.org/$LANGUAGE,
#   where $LANGUAGE will be the two-letter code for it.


# Little documentation for multi-language planet:

# In the following, above "Do not change", replace $LANGUAGE with the
# name of your language, $2LETTERCODE with the 2 letter iso code.
# For example, a german planet would use Deutsch and DE.
# Feel free to edit the other values as shown.
# Please do not touch the config values below "Do not change", just
# skip to the feed list.

# When you are done, send this file to planet@debian.org and ask for the
# addition of the new language planet. Do NOT just commit it, it won't get
# picked up by the scripts.

# After the new language is activated, feel free to edit this file following
# the normal rules for all planet config.ini files.

# Hint: You can use hackergotchis the same way as on main planet. But it is
# *one* central storage for hackergotchis, not one per different language.


# Every planet needs a [Planet] section
[Planet]
# name: Your planet's name
# link: Link to the main page
name = Planet Debian French
link = https://planet.debian.org/fr/
# language: short language code for the <html ... lang=""> tag
language = fr

# output_dir: Directory to place output files. Add your 2letter Language code
output_dir = www/fr
# date_format: strftime format for the default 'date' template variable
date_format = %d %B, %Y %I:%M%p
# new_date_format: strftime format for the 'new_date' template variable
new_date_format = %B %d, %Y
# cache_directory: Where cached feeds are stored. Add your 2letter Language code
cache_directory = cache/fr

# Translate the following to your language. Do not change the names, just the
# text after the =
syndicationtext = Un flux complet est disponible en de multiple formats via les boutons ci-dessous.
searchtext = Rechercher
lastupdatetext = Dernière mise à jour:
utctext = Tous les horaires sont en UTC.
contacttext = Contact:
hiddenfeedstext = Flux masqués
hiddenfeedstext2 = Vous avez actuellement des entrées masquées.
showalltext = Tout afficher
subscriptionstext = Abonnements
feedtext = flux
otherplanettext = Planetarium

# Do not change config values below here, just skip to the feeds
# Do not change config values below here, just skip to the feeds
owner_name = Debian Planet Maintainers
owner_email = planet@debian.org

# Currently no search for Language planets
search = false
# new_feed_items: Number of items to take from new feeds
# log_level: One of DEBUG, INFO, WARNING, ERROR or CRITICAL
new_feed_items = 5
log_level = DEBUG
spider_threads = 15

# template_files: Space-separated list of output template files
template_files = git/debian/templates/index.html.dj git/debian/templates/atom.xml.dj git/debian/templates/rss20.xml.dj git/debian/templates/rss10.xml.dj git/debian/templates/opml.xml.dj git/debian/templates/foafroll.xml.dj

# The following provide defaults for each template:
# items_per_page: How many items to put on each page
# days_per_page: How many complete days of posts to put on each page
#                This is the absolute, hard limit (over the item limit)
# encoding: output encoding for the file, Python 2.3+ users can use the
#           special "xml" value to output ASCII with XML character references
# locale: locale to use for (e.g.) strings in dates, default is taken from your
#         system
items_per_page = 60
days_per_page = 0
encoding = utf-8
# locale = C

filters = remove-trackers-and-ads.plugin
filter_dir = code/filters

[git/debian/templates/index.html.dj]
date_format = %I:%M%P

# Options placed in the [DEFAULT] section provide defaults for the feed
# sections.  Placing a default here means you only need to override the
# special cases later.
[DEFAULT]
# Hackergotchi default size.
# If we want to put a face alongside a feed, and it's this size, we
# can omit these variables.
facewidth = 65
faceheight = 85
future_dates = ignore_date

############################## FEEDS ##############################
#
# ADD YOURSELF IN ALPHABETICAL ORDER BELOW
#
###################################################################

# The URL of the feed goes in the []s.
# name = Your name
# face = filename of your hackergotchi in heads (or leave out entirely)
# facewidth/faceheight = size of your hackergotchi, if not default

[https://blog.aurel32.net/wp-rss2.php?cat=8]
name = Aur&#233;lien Jarno
face = aurel32.png
facewidth = 65
faceheight = 85

[http://carlchenet.wordpress.com/category/debian-fr/feed]
name = Carl Chenet

[http://charles.plessy.org/Debian/plan%C3%A8te/index.fr.rss]
name = Charles Plessy
face = plessy.png
facewidth = 65
faceheight = 85

# 2024-11-17 - 404
# [http://www.perrier.eu.org/weblog/bubulle/debian-fr/index.rss]
# name = Christian Perrier
# face = bubulle.png
# facewidth = 69
# faceheight = 85

[http://france.debian.net/blog/index.rss]
name = Debian France
face = debian-france.png
facewidth = 70
faceheight = 83

[http://blog.odyx.org/tag/fr/feed]
name = Didier Raboud

[http://fgallaire.flext.net/?feed=rss2]
name = Florent Gallaire

[http://gcolpart.evolix.net/blog21/category/debian-fr/feed/]
name = Gr&#233;gory Colpart
face = reg.png
#(face dispo. ici : http://gcolpart.evolix.net/pics/reg.png)

[http://blog.trankil.info/feed/tag/DebianPlanet/rss2]
name = J. Fernando Lagrange
face = lowmemory.png
facewidth = 70
faceheight = 70

# 2024-11-17 - NXDOMAIN
# [http://jean-christophe.dubacq.fr/superfeed/lang:fr/tag:debian?navlang=fr]
# name = Jean-Christophe Dubacq
# face = jcdubacq.png
# facewidth = 70
# faceheight = 90

#[http://julien.danjou.info/blog/rss.php?lang=fr&cat=Debian]
#name = Julien Danjou

[http://sathieu.wordpress.com/tag/planetdebianfr/feed/]
name = Mathieu Parent

# 2024-11-17 - 500
# [http://www-public.it-sudparis.eu/~berger_o/weblog/tag/debian-fr/feed/]
# name = Olivier Berger (pro)

# 2024-11-17 - 404
# [http://www.olivierberger.com/weblog/index.php?feed/tag/debian_fr/rss2]
# name = Olivier Berger (perso)

# 2024-11-17 - 404
# [http://machard.org/blog/index.php/archives/category/debianfr/feed/]
# name = Pierre Machard
# face = machard.png
# faceheight = 84

[http://raphaelhertzog.fr/feed/]
name = Rapha&#235;l Hertzog
face = hertzog.png
facewidth = 87
faceheight = 125

[http://roland.entierement.nu/categories/fr.rss]
name = Roland Mas
face = lolando.png
facewidth = 73
faceheight = 88

[http://ascendances.wordpress.com/category/debian/feed/]
name = Stéphane Blondon

[http://tanguy.ortolo.eu/blog/planet/debian-fr.rss]
name = Tanguy Ortolo
face = tanguy.png
facewidth = 71
faceheight = 62

[https://vincent.bernat.ch/fr/blog/atom.xml]
name = Vincent Bernat

# 2024-11-17 - 500
# [http://blog.developpez.com/xmlsrv/rss2.php?blog=322&cat=2647]
# name = Vincent Carmona
